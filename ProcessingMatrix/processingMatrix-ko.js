var ProcessingMatrixModel = function () {
    var self = this;
    //Processing matrix data
    self.items = ko.observableArray();
    self.columnNames = ko.observableArray();
    self.rowNames = ko.observableArray();

    //Fill rows and columns
    self.columnNames = ["", "Column1", "Column2", "Column3"];
    self.rowNames = ["Row1", "Row2", "Row3", "Row4"];
    
    self.rowNames.items = ko.observableArray();
    self.rowNames.items = ["aa", "bb", "cc"];
};


var ProcessingMatrix = function () {
    var self = this;
    self.shipmentTypes = ko.observableArray();
    self.deliveryMethods = ko.observableArray();
    self.items = ko.observableArray();
}

var ProcessingMatrixKo = function () {
    var self = this;
    self.head = ko.observableArray();
    self.rows = ko.observableArray();
    self.head = ["", "Column1", "Column2", "Column3"];
    self.rows = [["Row1", "data11", "data12", "data13"], 
                ["Row2", "data21", "data22", "data23"],
                ["Row3", "data31", "data32", "data33"],
                ["Row4", "data41", "data42", "data43"]];
};